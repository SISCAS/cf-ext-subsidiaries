<?php
/**
 * Subsidiaries - Dashboard
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

// redirect to subsidiaries list
api_redirect(api_url(["scr"=>"subsidiaries_list"]));