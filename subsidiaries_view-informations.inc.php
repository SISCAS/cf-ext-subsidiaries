<?php
/**
 * Subsidiaries - Subsidiaries View (Informations)
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var cSubsidiariesSubsidiary $subsidiary_obj
 */

// build informations description list
$informations_dl=new strDescriptionList("br","dl-horizontal");
$informations_dl->addElement(api_text("cSubsidiariesSubsidiaryTypology"),(new cSubsidiariesSubsidiaryTypology($subsidiary_obj->typology))->getLabel(true,true));
$informations_dl->addElement(api_text("cSubsidiariesSubsidiary-property-currency"),$subsidiary_obj->currency);
$informations_dl->addElement(api_text("subsidiaries_view-informations-dt-members"),count($subsidiary_obj->getMembers()));
