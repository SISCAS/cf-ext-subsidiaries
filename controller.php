<?php
/**
 * Subsidiaries - Controller
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

// debug
api_dump($_REQUEST,"_REQUEST");
// check if object controller function exists
if(function_exists($_REQUEST["obj"]."_controller")){
	// call object controller function
	call_user_func($_REQUEST["obj"]."_controller",$_REQUEST["act"]);
}else{
	api_alerts_add(api_text("alert_controllerObjectNotFound",[MODULE,$_REQUEST["obj"]."_controller"]),"danger");
	api_redirect("?mod=".MODULE);
}

/**
 * Role controller
 *
 * @param string $action Object action
 */
function cSubsidiariesRole_controller($action){
	// check authorizations
	api_checkAuthorization("subsidiaries-manage","dashboard");
	// get object
	$role_obj=new cSubsidiariesRole($_REQUEST["idRole"]);
	api_dump($role_obj,"role object");
	// check object
	if($action!="store" && !$role_obj->exists()){api_alerts_add(api_text("cSubsidiariesRole-alert-exists"),"danger");api_redirect(api_link(["scr"=>"management","tab"=>"roles"]));}
	// execution
	try{
		switch($action){
			case "store":
				$role_obj->store($_REQUEST);
				api_alerts_add(api_text("cSubsidiariesRole-alert-stored"),"success");
				break;
			case "move":
				$role_obj->move($_REQUEST["direction"]);
				api_alerts_add(api_text("cSubsidiariesRole-alert-moved"),"success");
				break;
			case "delete":
				$role_obj->delete();
				api_alerts_add(api_text("cSubsidiariesRole-alert-deleted"),"warning");
				break;
			case "undelete":
				$role_obj->undelete();
				api_alerts_add(api_text("cSubsidiariesRole-alert-undeleted"),"warning");
				break;
			case "remove":
				$role_obj->remove();
				api_alerts_add(api_text("cSubsidiariesRole-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Role action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management","tab"=>"roles","idRole"=>$role_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"roles","idRole"=>$role_obj->id]),"cSubsidiariesRole-alert-error");
	}
}

/**
 * Subsidiary controller
 *
 * @param string $action Object action
 */
function cSubsidiariesSubsidiary_controller($action){
	// check authorizations
	api_checkAuthorization("subsidiaries-manage","dashboard");
	// get object
	$subsidiary_obj=new cSubsidiariesSubsidiary($_REQUEST["idSubsidiary"]);
	api_dump($subsidiary_obj,"subsidiary object");
	// check object
	if($action!="store" && !$subsidiary_obj->exists()){api_alerts_add(api_text("cSubsidiariesSubsidiary-alert-exists"),"danger");api_redirect(api_url(["scr"=>"subsidiaries_list"]));}
	// execution
	try{
		switch($action){
			case "store":
				$subsidiary_obj->store($_REQUEST);
				api_alerts_add(api_text("cSubsidiariesSubsidiary-alert-stored"),"success");
				break;
			case "delete":
				$subsidiary_obj->delete();
				api_alerts_add(api_text("cSubsidiariesSubsidiary-alert-deleted"),"warning");
				break;
			case "undelete":
				$subsidiary_obj->undelete();
				api_alerts_add(api_text("cSubsidiariesSubsidiary-alert-undeleted"),"warning");
				break;
			case "remove":
				$subsidiary_obj->remove();
				api_alerts_add(api_text("cSubsidiariesSubsidiary-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Subsidiary action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"subsidiaries_list","idSubsidiary"=>$subsidiary_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"subsidiaries_list","idSubsidiary"=>$subsidiary_obj->id]),"cSubsidiariesSubsidiary-alert-error");
	}
}

/**
 * Subsidiary Member controller
 *
 * @param string $action Object action
 */
function cSubsidiariesSubsidiaryMember_controller($action){
	// check authorizations
	api_checkAuthorization("subsidiaries-manage","dashboard");
	// get object
	$member_obj=new cSubsidiariesSubsidiaryMember($_REQUEST["idMember"]);
	api_dump($member_obj,"subsidiary member object");
	// check object
	if($action!="store" && !$member_obj->exists()){api_alerts_add(api_text("cSubsidiariesSubsidiaryMember-alert-exists"),"danger");api_redirect(api_url(["scr"=>"subsidiaries_list"]));}
	// execution
	try{
		switch($action){
			case "store":
				$member_obj->store($_REQUEST);
				api_alerts_add(api_text("cSubsidiariesSubsidiaryMember-alert-stored"),"success");
				break;
			case "delete":
				$member_obj->delete();
				api_alerts_add(api_text("cSubsidiariesSubsidiaryMember-alert-deleted"),"warning");
				break;
			case "undelete":
				$member_obj->undelete();
				api_alerts_add(api_text("cSubsidiariesSubsidiaryMember-alert-undeleted"),"warning");
				break;
			case "remove":
				$member_obj->remove();
				api_alerts_add(api_text("cSubsidiariesSubsidiaryMember-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Subsidiary member action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"subsidiaries_view","tab"=>"members","idSubsidiary"=>$member_obj->fkSubsidiary,"idMember"=>$member_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"subsidiaries_view","tab"=>"members","idSubsidiary"=>$member_obj->fkSubsidiary,"idMember"=>$member_obj->id]),"cSubsidiariesSubsidiaryMember-alert-error");
	}
}
