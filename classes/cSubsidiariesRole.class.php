<?php
/**
 * Subsidiaries - Role
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries, Role class
 */
class cSubsidiariesRole extends cObject{

	/** Parameters */
	static protected $table="subsidiaries__roles";
	static protected $sortable=true;
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $order;
	protected $name_localized;
	protected $description_localized;

	/**
	 * Check
	 *
	 * @return boolean
	 * @throws Exception
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->name_localized))){throw new Exception("Role name is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Name
	 *
	 * @return string Role name localizated
	 */
	public function getName($localization_code=null){return api_text_localized($this->name_localized,$localization_code);}

	/**
	 * Get Description
	 *
	 * @return string Role description localizated
	 */
	public function getDescription($localization_code=null){return api_text_localized($this->description_localized,$localization_code);}

	/**
	 * Get Label
	 *
	 * @return string Role label
	 */
	public function getLabel($localization_code=null){
		// make label
		$label=$this->getName($localization_code);
		if($this->getDescription()){$label.=" (".$this->getDescription($localization_code).")";}
		// return
		return $label;
	}

	/**
	 * Get Members
	 *
	 * @param boolean $deleted Get also deleted entries
	 * @return object[]|false Array of entries objects or false
	 */
	public function getMembers($deleted=false){return cSubsidiariesSubsidiaryMember::availables($deleted,["fkRole"=>$this->id]);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"subsidiaries","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesRole","idRole"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_role_edit_form");
		// fields     /**  @todo valutare se tenere nome campo _localized o senza e come fare per il decode se fare funzione in cObject */
		$form->addField("text_localized","name_localized",api_text("cSubsidiariesRole-property-name"),json_decode($this->name_localized,true),api_text("cSubsidiariesRole-placeholder-name"),null,null,null,"required");
		$form->addField("text_localized","description_localized",api_text("cSubsidiariesRole-property-description"),json_decode($this->description_localized,true),api_text("cSubsidiariesRole-placeholder-description"),null,null,null,"rows='2'");
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * @inheritDoc
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Role remove function disabled by developer.. (enabled in debug mode)");}
		if(count($this->getMembers())){throw new Exception("Role remove function disabled for aready used roles");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
