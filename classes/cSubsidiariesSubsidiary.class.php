<?php
/**
 * Subsidiaries - Subsidiary
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries, Subsidiary class
 */
class cSubsidiariesSubsidiary extends cObject{

	/** Parameters */
	static protected $table="subsidiaries__subsidiaries";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $typology;
	protected $name;
	protected $description;
	protected $short;
	protected $currency;

	/**
	 * Check
	 *
	 * @return boolean
	 * @throws Exception
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->typology))){throw new Exception("Subsidiary, typology is mandatory..");}
		if(!strlen(trim($this->name))){throw new Exception("Subsidiary, name is mandatory..");}
		if(!strlen(trim($this->currency))){throw new Exception("Subsidiary, currency is mandatory..");}
		// return
		return true;
	}

	/**
	 * Decode log properties
	 *
	 * {@inheritdoc}
	 */
	public static function log_decode($event,$properties){
		// make return array
		$return_array=array();
		// subobject events
		if($properties["_obj"]=="cSubsidiariesSubsidiaryMember"){$return_array[]=api_text($properties["_obj"])." &rarr; ".(new cUser($properties["_fkUser"]))->fullname." (".(new cSubsidiariesRole($properties["_fkRole"]))->getName().")";}
		// return
		return implode(" | ",$return_array);
	}

	/**
	 * Get Label
	 *
	 * @return string Subsidiary label
	 */
	public function getLabel(){
		// make label
		$label=$this->name;
		if($this->description){$label.=" (".$this->description.")";}
		// return
		return $label;
	}

	/**
	 * Get Typology
	 *
	 * @return cSubsidiariesSubsidiaryTypology
	 */
	public function getTypology(){return new cSubsidiariesSubsidiaryTypology($this->typology);}

	/**
	 * Get Members
	 *
	 * @param integer|null $limit Limit number of members
	 * @param boolean $deleted Get also deleted members
	 *
	 * @return object[]|false Array of members objects or false
	 */
	public function getMembers($limit=null,$deleted=true,array $conditions=array()){return api_sortObjectsArray(cSubsidiariesSubsidiaryMember::availables($deleted,array_merge(["fkSubsidiary"=>$this->id],$conditions),$limit),"id",false);}

	/**
	 * Check if Subsidiary has given member
	 *
	 * @param $fkUser
	 * @return bool
	 */
	public function hasMember($fkUser){
		foreach($this->getMembers() as $member_fobj){
			if($member_fobj->fkUser==$fkUser){
				return true;
			}
		}
		return false;
	}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"subsidiaries","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesSubsidiary","idSubsidiary"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_subsidiary_edit_form");
		// fields
		$form->addField("select","typology",api_text("cSubsidiariesSubsidiary-property-typology"),$this->typology,api_text("cSubsidiariesSubsidiary-placeholder-typology"),null,null,null,"required");
		foreach(cSubsidiariesSubsidiaryTypology::availables() as $typology_fobj){$form->addFieldOption($typology_fobj->code,$typology_fobj->text);}
		$form->addField("text","name",api_text("cSubsidiariesSubsidiary-property-name"),$this->name,api_text("cSubsidiariesSubsidiary-placeholder-name"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cSubsidiariesSubsidiary-property-description"),$this->description,api_text("cSubsidiariesSubsidiary-placeholder-description"),null,null,null,"rows='2'");
		$form->addField("text","short",api_text("cSubsidiariesSubsidiary-property-short"),$this->short,api_text("cSubsidiariesSubsidiary-placeholder-short"));
		$form->addField("text","currency",api_text("cSubsidiariesSubsidiary-property-currency"),$this->currency,api_text("cSubsidiariesSubsidiary-placeholder-currency"));
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	// Disable remove function
	public function remove(){
		throw new Exception("Subsidiary remove function disabled by developer..");
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
