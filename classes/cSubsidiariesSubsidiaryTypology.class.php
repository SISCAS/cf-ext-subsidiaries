<?php
/**
 * Subsidiaries - Subsidiary Typology
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries, Subsidiary Typology class
 */
class cSubsidiariesSubsidiaryTypology extends cTranscoding{

	/** {@inheritdoc} */
	protected static function datas(){
		return array(
			["administrative",api_text("cSubsidiariesSubsidiaryTypology-administrative"),"fa-briefcase"],
			["commercial",api_text("cSubsidiariesSubsidiaryTypology-commercial"),"fa-file-text-o"],
			["productive",api_text("cSubsidiariesSubsidiaryTypology-productive"),"fa-cog"]
		);
	}

}
