<?php
/**
 * Subsidiaries - Subsidiary Member
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries, Subsidiary Member class
 */
class cSubsidiariesSubsidiaryMember extends cObject{

	/** Parameters */
	static protected $table="subsidiaries__subsidiaries__members";
	static protected $logs=false;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $fkSubsidiary;
	protected $fkUser;
	protected $fkRole;

	/**
	 * Check
	 *
	 * @return boolean
	 * @throws Exception
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->fkSubsidiary))){throw new Exception("Member subsidiary key is mandatory..");}
		if(!strlen(trim($this->fkUser))){throw new Exception("Member user key is mandatory..");}
		if(!strlen(trim($this->fkRole))){throw new Exception("Member role key is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Subsidiary
	 *
	 * @return cSubsidiariesSubsidiary
	 */
	public function getSubsidiary(){return new cSubsidiariesSubsidiary($this->fkSubsidiary);}

	/**
	 * Get User
	 *
	 * @return cUser
	 */
	public function getUser(){return new cUser($this->fkUser);}

	/**
	 * Get Role
	 *
	 * @return cSubsidiariesRole
	 */
	public function getRole(){return new cSubsidiariesRole($this->fkRole);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"subsidiaries","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesSubsidiaryMember","idMember"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_subsidiary_member_edit_form");
		// fields
		$form->addField("select","fkSubsidiary",api_text("cSubsidiariesSubsidiaryMember-property-fkSubsidiary"),$this->fkSubsidiary,api_text("cSubsidiariesSubsidiaryMember-placeholder-fkSubsidiary"),null,null,null,"required");
		foreach(cSubsidiariesSubsidiary::availables(true) as $subsidiary_fobj){$form->addFieldOption($subsidiary_fobj->id,$subsidiary_fobj->getLabel());}
		$form->addField("select","fkUser",api_text("cSubsidiariesSubsidiaryMember-property-fkUser"),$this->fkUser,api_text("cSubsidiariesSubsidiaryMember-placeholder-fkUser"),null,null,null,"required");
		foreach(api_availableUsers(true) as $user_fobj){$form->addFieldOption($user_fobj->id,$user_fobj->fullname." (".$user_fobj->mail.")");}
		/** @todo sistemare con nuova cFrameworkUser Extend cObject */
		$form->addField("select","fkRole",api_text("cSubsidiariesSubsidiaryMember-property-fkRole"),$this->fkRole,api_text("cSubsidiariesSubsidiaryMember-placeholder-fkRole"),null,null,null,"required");
		foreach(api_sortObjectsArray(cSubsidiariesRole::availables(true),"order") as $role_fobj){$form->addFieldOption($role_fobj->id,$role_fobj->getLabel());}
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Event Triggered
	 *
	 * {@inheritdoc}
	 */
	protected function event_triggered($event){
		//api_dump($event,static::class." event triggered");
		// skip trace events
		if($event->typology=="trace"){return;}
		// log event to subsidiary
		$this->getSubsidiary()->event_log($event->typology,$event->action,array_merge(["_obj"=>"cSubsidiariesSubsidiaryMember","_id"=>$this->id,"_fkUser"=>$this->fkUser,"_fkRole"=>$this->fkRole],$event->properties));
	}

}
