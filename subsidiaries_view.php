<?php
/**
 * Subsidiaries - Subsidiaries View
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries-usage","dashboard");
// get objects
$subsidiary_obj=new cSubsidiariesSubsidiary($_REQUEST["idSubsidiary"]);
// check objects
if(!$subsidiary_obj->exists()){api_alerts_add(api_text("cSubsidiariesSubsidiary-alert-exists"),"danger");api_redirect(api_url(["scr"=>"subsidiaries_list"]));}
// deleted alert
if($subsidiary_obj->deleted){api_alerts_add(api_text("cSubsidiariesSubsidiary-warning-deleted"),"warning");}
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("subsidiaries_view",$subsidiary_obj->name));
// build subsidiaries description list
$dl=new strDescriptionList("br","dl-horizontal");
$dl->addElement(api_text("cSubsidiariesSubsidiary-property-name"),api_tag("strong",$subsidiary_obj->name));
if($subsidiary_obj->description){$dl->addElement(api_text("cSubsidiariesSubsidiary-property-description"),nl2br($subsidiary_obj->description));}
if($subsidiary_obj->short){$dl->addElement(api_text("cSubsidiariesSubsidiary-property-short"),$subsidiary_obj->short);}
// check for tab
if(!defined(TAB)){define("TAB","informations");}
/**
 * Informations
 *
 * @var strDescriptionList $informations_dl
 */
require_once(MODULE_PATH."subsidiaries_view-informations.inc.php");
/**
 * Members
 *
 * @var strTable $members_table
 * @var cSubsidiariesSubsidiaryMember $selected_member_obj
 */
require_once(MODULE_PATH."subsidiaries_view-members.inc.php");
// build tabs
$tab=new strTab();
$tab->addItem(api_icon("fa-flag-o")." ".api_text("subsidiaries_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
$tab->addItem(api_icon("fa-users")." ".api_text("subsidiaries_view-tab-members"),$members_table->render(),("members"==TAB?"active":null));
$tab->addItem(api_icon("fa-file-text-o")." ".api_text("subsidiaries_view-tab-logs"),api_logs_table($subsidiary_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render(),("logs"==TAB?"active":null));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($dl->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($tab->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_member_obj){api_dump($selected_member_obj,"selected member");}
api_dump($subsidiary_obj,"subsidiary");
