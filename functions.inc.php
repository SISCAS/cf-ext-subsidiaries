<?php
/**
 * Subsidiaries Functions
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

// include classes
require_once(DIR."modules/subsidiaries/classes/cSubsidiariesRole.class.php");
require_once(DIR."modules/subsidiaries/classes/cSubsidiariesSubsidiary.class.php");
require_once(DIR."modules/subsidiaries/classes/cSubsidiariesSubsidiaryTypology.class.php");
require_once(DIR."modules/subsidiaries/classes/cSubsidiariesSubsidiaryMember.class.php");
