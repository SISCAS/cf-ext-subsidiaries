<?php
/**
 * Subsidiaries - Subsidiaries View (Members)
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cSubsidiariesSubsidiary $subsidiary_obj
 */

// build members table
$members_table=new strTable(api_text("subsidiaries_view-members-tr-unvalued"));
$members_table->addHeader(api_text("cSubsidiariesSubsidiaryMember-property-fkRole"),"nowrap");
$members_table->addHeader(api_text("cSubsidiariesSubsidiaryMember-property-fkUser"),"nowrap");
$members_table->addHeader(api_text("subsidiaries_view-members-th-mail"),null,"100%");  /** @todo variare quando disponibile cFrameworkUser-property-mail */
$members_table->addHeader(api_text("subsidiaries_view-members-th-lsaTimestamp"),"nowrap text-right");   /** @todo variare quando disponibile cFrameworkUser-property-lsaTimestamp */
if(api_checkAuthorization("subsidiaries-manage")){$members_table->addHeader("&nbsp;","nowrap");}
// cycle all members
foreach($subsidiary_obj->getMembers() as $member_fobj){
	// get user
	$user_fobj=new cUser($member_fobj->fkUser);
	// make table row class
	$tr_class_array=array();
	if($member_fobj->id==$_REQUEST["idMember"]){$tr_class_array[]="currentrow";}
	if($member_fobj->deleted){$tr_class_array[]="deleted";}
	// make row
	$members_table->addRow(implode(" ",$tr_class_array));
	$members_table->addRowField((new cSubsidiariesRole($member_fobj->fkRole))->getName(),"nowrap");
	$members_table->addRowField($user_fobj->fullname,"nowrap");
	$members_table->addRowField($user_fobj->mail,"truncate-ellipsis");
	$members_table->addRowField(api_timestamp_format($user_fobj->lsaTimestamp,api_text("datetime")),"nowrap text-right");
	// check for manage authorization
	if(api_checkAuthorization("subsidiaries-manage")){
		// build operation button
		$ob=new strOperationsButton();
		$ob->addElement(api_url(["scr"=>"subsidiaries_view","tab"=>"members","act"=>"member_edit","idSubsidiary"=>$subsidiary_obj->id,"idMember"=>$member_fobj->id]),"fa-pencil",api_text("table-td-edit"));
		$ob->addElement(api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesSubsidiaryMember","idSubsidiary"=>$subsidiary_obj->id,"idMember"=>$member_fobj->id,"return"=>["scr"=>"subsidiaries_view","tab"=>"members","idSubsidiary"=>$subsidiary_obj->id]]),"fa-trash",api_text("table-td-remove"),true,api_text("cSubsidiariesSubsidiaryMember-confirm-remove"));
		// add operation button to table
		$members_table->addRowField($ob->render(),"nowrap text-right");
	}
}

// check for actions
if(in_array(ACTION,["member_add","member_edit"]) && api_checkAuthorization("subsidiaries-manage")){
	// get selected member
	$selected_member_obj=new cSubsidiariesSubsidiaryMember($_REQUEST["idMember"]);
	// get form
	$form=$selected_member_obj->form_edit(["return"=>["scr"=>"subsidiaries_view","tab"=>"members","idSubsidiary"=>$subsidiary_obj->id]]);
	// replace fkSubsidiary
	$form->removeField("fkSubsidiary");
	$form->addField("hidden","fkSubsidiary",null,$subsidiary_obj->id);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_member_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesSubsidiaryMember","idSubsidiary"=>$subsidiary_obj->id,"idMember"=>$selected_member_obj->id,"return"=>["scr"=>"subsidiaries_view","tab"=>"members","idSubsidiary"=>$subsidiary_obj->id]]),"btn-danger",api_text("cSubsidiariesSubsidiaryMember-confirm-remove"));}
	// build modal
	$modal=new strModal(api_text("subsidiaries_view-members-modal-title-".($selected_member_obj->exists()?"edit":"add"),$subsidiary_obj->name),null,"subsidiaries_view-members");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_subsidiaries_view-members').modal({show:true,backdrop:'static',keyboard:false});});");
	$app->addScript("$(document).ready(function(){\$('select[name=\"fkUser\"]').select2({allowClear:true,placeholder:\"".api_text("cSubsidiariesSubsidiaryMember-placeholder-fkUser")."\",dropdownParent:\$('#modal_subsidiaries_view-members')});});");
}
