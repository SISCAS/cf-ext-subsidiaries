<?php
/**
 * Subsidiaries
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// definitions
$module_name="subsidiaries";
$module_repository_url="https://bitbucket.org/SISCAS/cf-ext-subsidiaries/";
$module_repository_version_url="https://bitbucket.org/SISCAS/cf-ext-subsidiaries/raw/master/VERSION.txt";
$module_required_modules=array();
