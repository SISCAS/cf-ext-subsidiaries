<?php
/**
 * Subsidiaries - Template
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 */

// build application
$app=new strApplication();
// build nav
$nav=new strNav("nav-tabs");
// dashboard
$nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
// management
if(api_checkAuthorization("subsidiaries-manage")){$nav->addItem(api_text("nav-management"),api_url(["scr"=>"management"]));}

/**
 * Subsidiaries
 *
 * @var cSubsidiariesSubsidiary $subsidiary_obj
 */
$nav->addItem(api_text("nav-subsidiaries-list"),api_url(["scr"=>"subsidiaries_list"]));
if(api_script_prefix()=="subsidiaries"){
	if(SCRIPT=="subsidiaries_edit" && !$subsidiary_obj->exists()){$nav->addItem(api_text("nav-subsidiaries-add"),api_url(["scr"=>"subsidiaries_edit"]),(api_checkAuthorization("subsidiaries-manage")));}
	elseif(is_object($subsidiary_obj) && $subsidiary_obj->exists() && in_array(SCRIPT,array("subsidiaries_view","subsidiaries_edit"))){
		$nav->addItem(api_text("nav-operations"),null,null,"active");
		$nav->addSubItem(api_text("nav-subsidiaries-operations-edit"),api_url(["scr"=>"subsidiaries_edit","idSubsidiary"=>$subsidiary_obj->id]),(api_checkAuthorization("subsidiaries-manage")));
		$nav->addSubSeparator();$nav->addSubItem(api_text("nav-subsidiaries-operations-member_add"),api_url(["scr"=>"subsidiaries_view","tab"=>"members","act"=>"member_add","idSubsidiary"=>$subsidiary_obj->id]),(api_checkAuthorization("subsidiaries-manage")));
	}
}

// add nav to html
$app->addContent($nav->render(false));
