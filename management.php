<?php
/**
 * Subsidiaries - Management
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries-manage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("management"));
// check for tab
if(!defined(TAB)){define("TAB","roles");}
// build navigation
$nav=new strNav("nav-pills");
$nav->addItem(api_text("management-nav-roles"),api_url(["scr"=>"management","tab"=>"roles"]));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($nav->render(false),"col-xs-12");
$grid->addRow();
// switch tab
switch(TAB){
	/**
	 * Roles
	 *
	 * @var strTable $roles_table
	 * @var cSubsidiariesRole $selected_role_obj
	 */
	case "roles":
		// include tab
		require_once(MODULE_PATH."management-roles.inc.php");
		$grid->addCol($roles_table->render(),"col-xs-12");
		break;
}
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_role_obj){api_dump($selected_role_obj,"selected role");}