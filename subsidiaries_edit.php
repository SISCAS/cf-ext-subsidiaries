<?php
/**
 * Subsidiaries - Subsidiaries Edit
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries-manage","dashboard");
// get objects
$subsidiary_obj=new cSubsidiariesSubsidiary($_REQUEST["idSubsidiary"]);
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(($subsidiary_obj->exists()?api_text("subsidiaries_edit",$subsidiary_obj->name):api_text("subsidiaries_edit-new")));
// get form
$form=$subsidiary_obj->form_edit(["return"=>api_return(["scr"=>"subsidiaries_view"])]);
// additional controls
if($subsidiary_obj->exists()){
	$form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"subsidiaries_view","idSubsidiary"=>$subsidiary_obj->id]));
	if(!$subsidiary_obj->deleted){
		$form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cSubsidiariesSubsidiary","idSubsidiary"=>$subsidiary_obj->id]),"btn-danger",api_text("cSubsidiariesSubsidiary-confirm-delete"));
	}else{
		$form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesSubsidiary","idSubsidiary"=>$subsidiary_obj->id,"return"=>["scr"=>"subsidiaries_view"]]),"btn-warning");
		$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesSubsidiary","idSubsidiary"=>$subsidiary_obj->id]),"btn-danger",api_text("cSubsidiariesSubsidiary-confirm-remove"));
	}
}else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"subsidiaries_list"]));}
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($form->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($subsidiary_obj,"subsidiary");
