--
-- Subsidiaries - Update (1.0.2)
--
-- @package Coordinator\Modules\Subsidiaries
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `subsidiaries__subsidiaries`
--

ALTER TABLE `subsidiaries__subsidiaries` CHANGE COLUMN `name` `name` VARCHAR(128) NOT NULL;
ALTER TABLE `subsidiaries__subsidiaries` ADD COLUMN `short` VARCHAR(16) NULL DEFAULT NULL AFTER `description`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
