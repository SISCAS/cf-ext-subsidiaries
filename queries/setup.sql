--
-- Subsidiaries - Setup (1.0.0)
--
-- @package Coordinator\Modules\Subsidiaries
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries__roles`
--

CREATE TABLE IF NOT EXISTS `subsidiaries__roles` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`order` int(11) unsigned NOT NULL,
	`name_localized` text COLLATE utf8_unicode_ci NOT NULL,
	`description_localized` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries__roles__logs`
--

CREATE TABLE IF NOT EXISTS `subsidiaries__roles__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `subsidiaries__roles__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `subsidiaries__roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries__subsidiaries`
--

CREATE TABLE IF NOT EXISTS `subsidiaries__subsidiaries` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`typology` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries__subsidiaries__logs`
--

CREATE TABLE IF NOT EXISTS `subsidiaries__subsidiaries__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `subsidiaries__subsidiaries__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `subsidiaries__subsidiaries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries__subsidiaries__members`
--

CREATE TABLE IF NOT EXISTS `subsidiaries__subsidiaries__members` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkSubsidiary` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned NOT NULL,
	`fkRole` int(11) unsigned NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `fkSubsidiary_fkUser` (`fkSubsidiary`,`fkUser`),
	KEY `fkSubsidiary` (`fkSubsidiary`),
	KEY `fkUser` (`fkUser`),
	KEY `fkRole` (`fkRole`),
	CONSTRAINT `subsidiaries__subsidiaries__members_ibfk_1` FOREIGN KEY (`fkSubsidiary`) REFERENCES `subsidiaries__subsidiaries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `subsidiaries__subsidiaries__members_ibfk_2` FOREIGN KEY (`fkUser`) REFERENCES `framework__users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `subsidiaries__subsidiaries__members_ibfk_3` FOREIGN KEY (`fkRole`) REFERENCES `subsidiaries__roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('subsidiaries-manage','subsidiaries',1),
('subsidiaries-usage','subsidiaries',2);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
