--
-- Subsidiaries - Update (1.0.5)
--
-- @package Coordinator\Modules\Subsidiaries
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `subsidiaries__subsidiaries`
--

ALTER TABLE `subsidiaries__subsidiaries` ADD COLUMN `currency` VARCHAR(3) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `short`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
