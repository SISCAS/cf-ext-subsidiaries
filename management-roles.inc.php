<?php
/**
 * Subsidiaries - Management (Roles)
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build roles table
$roles_table=new strTable(api_text("management-roles-tr-unvalued"));
$roles_table->addHeader(api_text("cSubsidiariesRole-property-name"),"nowrap");
$roles_table->addHeader(api_text("cSubsidiariesRole-property-description"),null,"100%");
$roles_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"roles","act"=>"role_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all roles
foreach(api_sortObjectsArray(cSubsidiariesRole::availables(true),"order") as $role_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"management","tab"=>"roles","act"=>"role_view","idRole"=>$role_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"controller","act"=>"move","direction"=>"up","obj"=>"cSubsidiariesRole","idRole"=>$role_fobj->id,"return"=>["scr"=>"management","tab"=>"roles"]]),"fa-arrow-up",api_text("table-td-move-up"),($role_fobj->order>1));
	$ob->addElement(api_url(["scr"=>"controller","act"=>"move","direction"=>"down","obj"=>"cSubsidiariesRole","idRole"=>$role_fobj->id,"return"=>["scr"=>"management","tab"=>"roles"]]),"fa-arrow-down",api_text("table-td-move-down"),($role_fobj->order!=cSubsidiariesRole::count(true)));
	$ob->addElement(api_url(["scr"=>"management","tab"=>"roles","act"=>"role_edit","idRole"=>$role_fobj->id]),"fa-pencil",api_text("table-td-edit"));
	$ob->addElement(api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesRole","idRole"=>$role_fobj->id,"return"=>["scr"=>"management","tab"=>"roles"]]),"fa-trash",api_text("table-td-remove"),true,api_text("cSubsidiariesRole-confirm-remove"));
	// make table row class
	$tr_class_array=array();
	if($role_fobj->id==$_REQUEST["idRole"]){$tr_class_array[]="currentrow";}
	if($role_fobj->deleted){$tr_class_array[]="deleted";}
	// make subsidiaries row
	$roles_table->addRow(implode(" ",$tr_class_array));
	$roles_table->addRowField($role_fobj->getName(),"nowrap");
	$roles_table->addRowField($role_fobj->getDescription(),"truncate-ellipsis");
	$roles_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="role_view"){
	// get selected role
	$selected_role_obj=new cSubsidiariesRole($_REQUEST["idRole"]);
	// build description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cSubsidiariesRole-property-name"),api_tag("strong",$selected_role_obj->getName()));
	if($selected_role_obj->getDescription()){$dl->addElement(api_text("cSubsidiariesRole-property-description"),$selected_role_obj->getDescription());}
	// build modal
	$modal=new strModal(api_text("management-roles-modal-title-view",$selected_role_obj->getName()),null,"management-roles-view");
	$modal->setBody($dl->render()."<hr>".api_logs_table($selected_role_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-roles-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["role_add","role_edit"])){
	// get selected role
	$selected_role_obj=new cSubsidiariesRole($_REQUEST["idRole"]);
	// get form
	$form=$selected_role_obj->form_edit(["return"=>["scr"=>"management","tab"=>"roles"]]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_role_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesRole","idRole"=>$selected_role_obj->id,"return"=>["scr"=>"management","tab"=>"roles"]]),"btn-danger",api_text("cSubsidiariesRole-confirm-remove"));}
	// build modal
	$modal=new strModal(api_text("management-roles-modal-title-".($selected_role_obj->exists()?"edit":"add"),$selected_role_obj->getName()),null,"management-roles-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-roles-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
