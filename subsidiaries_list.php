<?php
/**
 * Subsidiaries - Subsidiaries List
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries-usage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("subsidiaries_list"));
// definitions
$subsidiaries_array=array();
// build filter
$filter=new strFilter();
$filter->addSearch(["name","description"]);
$filter->addItem(api_text("cSubsidiariesSubsidiaryTypology"),api_transcodingsFromObjects(cSubsidiariesSubsidiaryTypology::availables(),"code","text"),"typology");
// build query
$query=new cQuery("subsidiaries__subsidiaries",$filter->getQueryWhere());
$query->addQueryOrderField("name");
// build pagination
$pagination=new strPagination($query->getRecordsCount());
// cycle all results
foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$subsidiaries_array[$result_f->id]=new cSubsidiariesSubsidiary($result_f);}
// build table
$table=new strTable(api_text("subsidiaries_list-tr-unvalued"));
$table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
$table->addHeader(api_text("cSubsidiariesSubsidiary-property-name"),"nowrap");
$table->addHeader("&nbsp;","nowrap");
$table->addHeader(api_text("cSubsidiariesSubsidiary-property-description"),null,"100%");
if(api_checkAuthorization("subsidiaries-manage")){$table->addHeaderAction(api_url(["scr"=>"subsidiaries_edit"]),"fa-plus",api_text("table-td-add"),null,"text-right");}
// cycle all subsidiaries
foreach($subsidiaries_array as $subsidiary_fobj){
	// make table row class
	$tr_class_array=array();
	if($subsidiary_fobj->id==$_REQUEST["idSubsidiary"]){$tr_class_array[]="currentrow";}
	if($subsidiary_fobj->deleted){$tr_class_array[]="deleted";}
	// make subsidiaries row
	$table->addRow(implode(" ",$tr_class_array));
	$table->addRowFieldAction(api_url(["scr"=>"subsidiaries_view","idSubsidiary"=>$subsidiary_fobj->id]),"fa-search",api_text("table-td-view"));
	$table->addRowField($subsidiary_fobj->name,"nowrap");
	$table->addRowField($subsidiary_fobj->getTypology()->getLabel(false,true),"nowrap");
	$table->addRowField($subsidiary_fobj->description,"truncate-ellipsis");
	// check for manage authorization
	if(api_checkAuthorization("subsidiaries-manage")){
		// build operation button
		$ob=new strOperationsButton();
		$ob->addElement(api_url(["scr"=>"subsidiaries_edit","idSubsidiary"=>$subsidiary_fobj->id,"return"=>["scr"=>"subsidiaries_list"]]),"fa-pencil",api_text("table-td-edit"));
		if($subsidiary_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesSubsidiary","idSubsidiary"=>$subsidiary_fobj->id,"return"=>["scr"=>"subsidiaries_list"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cSubsidiariesSubsidiary-confirm-undelete"));}
		else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cSubsidiariesSubsidiary","idSubsidiary"=>$subsidiary_fobj->id,"return"=>["scr"=>"subsidiaries_list"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cSubsidiariesSubsidiary-confirm-delete"));}
		// add operation button to table
		$table->addRowField($ob->render(),"nowrap text-right");
	}
}
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($filter->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($pagination->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($query,"query");
